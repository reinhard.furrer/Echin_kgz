---
title: "descriptive"
author: "Giulia"
date: "5/1/2019"
output:
  html_document: default
  pdf_document: default
---


```{r global_options, include=FALSE}
knitr::opts_chunk$set(linewidth = 60, echo= T, warning=FALSE, message=FALSE)
rm(list=ls())
```

```{r setup}
knitr::opts_knit$set(root.dir = "/Volumes/Giulia/EBPhD/PAPERS/SPATIAL_echinococcusKGZ/SENT_coauthors_300519/repo_kgz")
```

# Project
## Epidemic cystic and alveolar echinococcosis in Kyrgyzstan???high resolution maps using national surveillance data identify disease hotspots

## Contains R code used for descriptive statistics on the population of 490 map administrative units (appendix) which include
- 476 communities (adm3)
- 12 independent cities (adm2)
- 2 cities Bishkek and Osh (adm1)
-> Supplementary fugures in the appendix

```{r upload,results="hide", message=FALSE, warning=FALSE}

#LIBRARIES
###LOAD LIBRARIES###
install.packages("spatialEco")
install.packages("tcltk")
library(tmap); library(rgdal); library(raster); library(plyr); 
library(sp); library(rgeos); library(spatialEco); library(tcltk)
library(spdep)
library(rgdal)
library(plyr)
library(rgeos)
library(fields)
library(rgdal)
library(MASS)
library(sp)
library(sf)
library(plyr)
library(readxl)
library(stringr)
library(spData)
library(Matrix)
library(splancs)
library(tidyverse)
library(tmap)
library(rworldmap)
library(geosphere)
library(ggmap)
library(raster)
library(tmaptools)


#DATASETS

#HUMAN CASES
cases <- read.csv("DATA/KGZ_ancillary/TotalListGIS2g_31.7small.csv")[,1:20]
colnames(cases)
cases <- cases[1:3168,c("ID","Sex_correct","Age","DIAGNOSIS_def",
                        "RAYON_upd","lat_upd","long_upd")]
colnames(cases) <- c("id","sex","age","diag","adm2","lat","long")


#POPULATION
pop <- ADM3_ADM2_NAMES14_8 <-read_excel("DATA/KGZ_ancillary/270818_AdminUnit_FINAL.xlsx", sheet = "270818_AdminUnit_FINAL")
pop <- pop[!is.na(pop$OBJECTID),] 
# delete (three) adm that are missing from map
colnames(pop) <- c("wkt_geom","OBJECTID","name_r","name_e","adm pcode","NAME_rus", "NAME_en","ADM","adminType","admin1Name_en","admin2Name_en","descr census 2009","pop census 2009","checked","edited","flag","notes 27.8.18")

#GEODATA
admin_0 <- readOGR("DATA/KGZ_admin0","KGZ_adm0")
admin_1 <- readOGR("DATA/KGZ_admin1","reach_KGZ.SDE.KGZ_ADM_admin_1")
admin_3 <- spTransform(readOGR("DATA/KGZ_admin3", "270818_AdminUnit_FINAL"), CRS("+proj=longlat +datum=WGS84"))
shape <- readOGR("DATA/KGZ_echino", "shape_attributesADM3")
elevation <- raster("DATA/KGZ_altitude/KGZ_alt.tif")
water_areas <- readOGR("DATA/KGZ_water_areas", "KGZ_water_areas_dcw")

admin_1 <- spTransform(admin_1, CRS(paste(crs(elevation))))
elevation <- raster::mask(elevation, admin_1)
admin_1 <- remove.holes(admin_1)

# age imputation for 19 individuals
estimate_mode <- function(x) {
  d <- density(x)
  d$x[which.max(d$y)]
}
cases.missing <- which(!complete.cases(cases))
cases[cases.missing,]
for (i in cases.missing){
  cases$age[i] <- estimate_mode(na.exclude
    (subset(cases, sex == cases$sex[i] & diag == cases$diag[i])$age))
}
cases[cases.missing,]

```

# Population data 490 administrative units: maps and plots

```{r pop, results="hide", message=FALSE, warning=FALSE, chunk_output_type: console}

#Process population data
admin_3@data$OBJECTID <- as.character(admin_3@data$OBJECTID)
temp <- merge(admin_3@data, pop[,c("OBJECTID","adm pcode","NAME_en","pop census 2009","admin1Name_en","admin2Name_en")], by = "OBJECTID", all.x = TRUE, all.y = TRUE, sort = FALSE)
temp[is.na(temp$checked)|is.na(temp$`pop census 2009`),]
# three missing. see note in original excel
temp[temp$name_e.x != temp$name_e.y,]
# another check to see if any names mismatch
admin_3@data <- temp[1:nrow(admin_3@data),]
admin_3@data$id <- rownames(admin_3@data)
admin_3.points <- fortify(admin_3, region="id")
admin_3.df <- join(admin_3.points, admin_3@data, by="id")

ggplot(admin_3.df) +
  geom_polygon(data = admin_3, aes(x = long, y = lat, group = group), fill = "white", color = "white")+
  geom_polygon(aes(x = long, y = lat, group = group,fill=log(`pop census 2009`))) +
  coord_equal()


#population map
#install.packages("BAMMtools")
#Given a vector of numeric values and the number of desired breaks, calculate the optimum
#breakpoints using Jenks natural breaks optimization.
library(BAMMtools)
getJenksBreaks(shape@data$pop, 6, subset = NULL)


Popmap <- 
  tm_layout(main.title="Population 490 administrative units", main.title.size=1.2, frame=F, fontfamily = "Avenir Next", legend.position = c("right","bottom")) +
  tm_shape(admin_0) +
  tm_borders(col="black", lwd=3) +
  # tm_shape(admin_1) +
  # tm_fill(col="white") +
  # tm_borders(col="black", lwd=1) +
  tm_shape(admin_3) +
  tm_fill(col="pop census 2009", palette="Reds", breaks=c(125,10000,30000,90000,260000,max(admin_3@data$`pop census 2009`)),
  title = "Population\nper administrative unit", legend.reverse=T) +
  tm_borders(col="black", lwd=0.5) +
  tm_shape(water_areas) +
  tm_fill("#a4d3ee") +
  tm_borders(col="#a4d3ee", lwd=0.5) +
  tm_shape(admin_0) +
  tm_borders(col="black", lwd=3) 

Popmap
tmap_save(Popmap, "./kgz_mapping/graphics/Popmap.png", width=4000, dpi=300)

```

#Population descriptive
```{r pop.plots, message=FALSE, warning=FALSE}

#descriptive
summary(shape@data$pop)
df <- shape@data

# Basic histogram
library(scales)
ggplot(df, aes(x=pop)) + 
  geom_histogram() +
  scale_x_continuous(name="population", labels = scales::comma)

#create population classes with same breaks as map (using Jerkin )
df$popclass <- cut(x = df$pop, breaks = c(-Inf, 10000, 30000, 90000, 260000, Inf), 
                   labels = c("125-10000", "10000-30000", "30000-90000", "258111, Osh city", "835743, Bishkek city"))


library(dplyr)
df <- df[,c("OBJECTID","adm3","pop","popclass")]
# df.grouped <- df %>%
#   group_by(popclass) %>% summarise(counts = n())
# df.grouped
# #sort by COUNT (descending)
# df.grouped  <- df.grouped[order(-df.grouped$counts),] 
# df.grouped$perc <- df.grouped$counts/sum(df.grouped$counts)

#barplot
library(ggpubr)
barplotpop <- 
  ggplot(df, aes(x = popclass)) +
  # ggtitle("Population of 490 administrative units") +
  geom_bar(fill = "#0073C2FF") +
  geom_text(stat = "count", aes(label = ..count..), vjust = -0.3) + 
  scale_x_discrete("Pupulation class") +
  theme_classic() 


#ggsave("./barplotpop.tiff", plot = barplotpop, device = NULL, path = NULL, scale = 1, dpi = 300, limitsize = TRUE, width = NA, height = NA)

```

# Area 490 administrative units in square Km

```{r areaadm3, message=FALSE, warning=FALSE}

#area in Square Km 
admin_3$areasqkm <- (raster::area(admin_3,byid=TRUE)) / 1000000
summary(admin_3$areasqkm)

area <- admin_3@data

areaadmin.barplot <- 
  ggplot(area, aes(x = areasqkm), fill = "#0073C2FF", stat = "identity") + 
  geom_histogram(binwidth = 50) +
  scale_x_continuous(name="area (sq.km)", labels = scales::comma, 
                     breaks = seq(0, 1500, 150)) +
  labs(fill = "#0073C2FF") +
  #theme_hc()
  
areaadmin.barplot

#ggsave("./areaadmin.barplot.tiff", plot = areaadmin.barplot, device = NULL, path = NULL, scale = 1, dpi = 300, limitsize = TRUE, width = NA, height = NA)

```

