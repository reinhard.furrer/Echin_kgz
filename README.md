# Echin_kgz

## Epidemic cystic and alveolar echinococcosis in Kyrgyzstan—high resolution maps using national surveillance data identify disease hotspots

### Contains R scripts used for generating data and maps
### Authors: Giulia Paternoster, Craig Wang, Gianluca Boo
### The scripts are for reference only, data is not shared. The shapefile shared is for humanitarian use only
